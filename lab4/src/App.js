import React from 'react';
import ProductName from "./components/ProductName";
import { Container, Paper, TableContainer, Table, TableBody } from "@mui/material";
import Product from "./components/Product";
import { ProductsProvider } from "./context/products-context";

function App() {
  const [name, setName] = React.useState('');

  const handleNameChange = (newName) => {
    setName(newName);
  };

  const handleFormSubmit = () => {
    // Your form submission logic goes here
  };

  const handleDelete = (id) => {
    // Logic to delete a product goes here
  };

  const handleUpdate = (id, updatedName) => {
    // Logic to update a product goes here
  };

  return (
      <div className="App">
        <Container>
          <Paper elevation={3} style={{ padding: '16px' }}>
            <ProductName name={name} onNameChange={handleNameChange} onFormSubmit={handleFormSubmit} />

            <TableContainer component={Paper}>
              <Table>
                <TableBody>
                  {/* Example of using the Product component */}
                  {/* Replace this with actual logic to map over products */}
                  <Product
                      product={{ id: 1, name: 'Mouse' }}
                      onDelete={handleDelete}
                      onUpdate={handleUpdate}
                  />
                  <Product
                      product={{ id: 1, name: 'Laptop' }}
                      onDelete={handleDelete}
                      onUpdate={handleUpdate}
                  />
                  <Product
                      product={{ id: 1, name: 'Keybord' }}
                      onDelete={handleDelete}
                      onUpdate={handleUpdate}
                  />
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </Container>
      </div>
  );
}

const AppWrapper = () => {
  return (
      <ProductsProvider>
        <App />
      </ProductsProvider>
  );
};

export default AppWrapper;