import React, { createContext, useReducer, useContext, useEffect } from 'react';
import { productsReducer } from '../reducers/products';

export const ProductsContext = createContext();

export const ProductsProvider = ({ children }) => {
    const [products, dispatch] = useReducer(productsReducer, []);

    useEffect(() => {
        const storedProducts = localStorage.getItem('products');
        if (storedProducts) {
            dispatch({ type: 'POPULATE_PRODUCTS', payload: JSON.parse(storedProducts) });
        }
    }, []);

    useEffect(() => {
        localStorage.setItem('products', JSON.stringify(products));
    }, [products]);

    return (
        <ProductsContext.Provider value={{ products, dispatch }}>
            {children}
        </ProductsContext.Provider>
    );
};

export const useProducts = () => {
    const context = useContext(ProductsContext);
    if (!context) {
        throw new Error('useProducts must be used within a ProductsProvider');
    }
    return context;
};