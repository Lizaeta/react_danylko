import React, { useState, useEffect } from 'react';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import TextField from '@mui/material/TextField';

const Product = ({ product, onDelete, onUpdate }) => {
    const [isEditing, setIsEditing] = useState(false);
    const [editedName, setEditedName] = useState(product.name);

    useEffect(() => {
        setEditedName(product.name);
    }, [product.name]);

    const handleEditToggle = () => {
        setIsEditing(!isEditing);
    };

    const handleNameChange = (e) => {
        setEditedName(e.target.value);
    };

    const handleUpdate = () => {
        onUpdate(product.id, editedName);
        setIsEditing(false);
    };

    return (
        <TableRow key={product.id}>
            <TableCell>
                {isEditing ? (
                    <TextField
                        value={editedName}
                        onChange={handleNameChange}
                        fullWidth
                        variant="outlined"
                    />
                ) : (
                    product.name
                )}
            </TableCell>
            <TableCell align="right">
                {isEditing ? (
                    <Button variant="contained" color="primary" onClick={handleUpdate}>
                        Save
                    </Button>
                ) : (
                    <>
                        <Button variant="contained" color="secondary" onClick={() => onDelete(product.id)}>
                            <DeleteIcon />
                        </Button>
                        <Button variant="contained" color="primary" onClick={handleEditToggle}>
                            <EditIcon />
                        </Button>
                    </>
                )}
            </TableCell>
        </TableRow>
    );
};

export default Product;