import React from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

const ProductName = ({ name, onNameChange, onFormSubmit }) => {
    const handleFormSubmit = (e) => {
        e.preventDefault();
        onFormSubmit();
    };

    return (
        <form onSubmit={handleFormSubmit}>
            <TextField
                label="Product Name"
                variant="outlined"
                fullWidth
                value={name}
                onChange={(e) => onNameChange(e.target.value)}
            />
            <Button variant="contained" color="primary" type="submit" style={{ marginTop: '16px' }}>
                Add to List
            </Button>
        </form>
    );
};

export default ProductName;