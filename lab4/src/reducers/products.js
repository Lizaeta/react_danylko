export const ActionTypes = {
    POPULATE_PRODUCTS: 'POPULATE_PRODUCTS',
    ADD_PRODUCT: 'ADD_PRODUCT',
    REMOVE_PRODUCT: 'REMOVE_PRODUCT',
    EDIT_PRODUCT: 'EDIT_PRODUCT',
};

export const productsReducer = (state, action) => {
    switch (action.type) {
        case ActionTypes.POPULATE_PRODUCTS:
            return action.payload;

        case ActionTypes.ADD_PRODUCT:
            return [...state, action.payload];

        case ActionTypes.REMOVE_PRODUCT:
            return state.filter(product => product.id !== action.payload);

        case ActionTypes.EDIT_PRODUCT:
            return state.map(product =>
                product.id === action.payload.id ? { ...product, name: action.payload.name } : product
            );

        default:
            return state;
    }
};