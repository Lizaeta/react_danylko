import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import styled from 'styled-components';

const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  max-width: 300px;
  margin: 0 auto;
`;

const FormGroup = styled.div`
  margin-bottom: 15px;
`;

const Label = styled.label`
  margin-bottom: 5px;
`;

const Input = styled.input`
  padding: 8px;
  border-radius: 5px;
  border: 1px solid #ccc;
`;

const Checkbox = styled.input`
  margin-right: 5px;
`;

const ErrorMsg = styled.p`
  color: #ff0000;
  margin-top: 5px;
  font-size: 14px;
`;

const cities = ['Київ', 'Житомир', 'Харків', 'Львів', 'Рівне'];
const shippingTypes = ['Вантажні', 'Палети'];

const schema = yup.object().shape({
    cityFrom: yup.string().required('Місто-відправник є обов\'язковим полем'),
    cityTo: yup.string().required('Місто-одержувач є обов\'язковим полем'),
    shippingType: yup.string().required('Вид відправлення є обов\'язковим полем'),
    places: yup.array().of(
        yup.object().shape({
            quantity: yup.number().typeError('Кількість має бути числом').required('Кількість є обов\'язковим полем'),
            packaging: yup.number().typeError('Пакування має бути числом').required('Пакування є обов\'язковим полем'),
        })
    ),
    lift: yup.object().shape({
        floors: yup.number().typeError('Кількість поверхів має бути числом'),
        elevator: yup.boolean(),
    }),
    returnDelivery: yup.boolean(),
});

const NovaForm = () => {
    const { register, handleSubmit, reset, formState: { errors }, getValues } = useForm({
        resolver: yupResolver(schema),
        defaultValues: {
            places: [{ quantity: '', packaging: '' }],
            lift: { floors: '', elevator: false },
            returnDelivery: false,
        },
    });

    const [placesCount, setPlacesCount] = useState(1);

    const onSubmit = (data) => {
        console.log(data);
    };

    const handleReset = () => {
        reset();
    };

    const addPlace = () => {
        setPlacesCount(placesCount + 1);
        const currentPlaces = getValues().places;
        const newPlace = { quantity: '', packaging: '' };
        reset({ places: [...currentPlaces, newPlace] });
    };

    return (
        <FormContainer onSubmit={handleSubmit(onSubmit)}>
            <FormGroup>
                <Label htmlFor="cityFrom">Місто-відправник:</Label>
                <select id="cityFrom" {...register('cityFrom')}>
                    <option value="">Оберіть місто</option>
                    {cities.map((city) => (
                        <option key={city} value={city}>
                            {city}
                        </option>
                    ))}
                </select>
                {errors.cityFrom && <ErrorMsg>{errors.cityFrom.message}</ErrorMsg>}
            </FormGroup>
            <FormGroup>
                <Label htmlFor="cityTo">Місто-одержувач:</Label>
                <select id="cityTo" {...register('cityTo')}>
                    <option value="">Оберіть місто</option>
                    {cities.map((city) => (
                        <option key={city} value={city}>
                            {city}
                        </option>
                    ))}
                </select>
                {errors.cityTo && <ErrorMsg>{errors.cityTo.message}</ErrorMsg>}
            </FormGroup>
            <FormGroup>
                <Label htmlFor="shippingType">Вид відправлення:</Label>
                <select id="shippingType" {...register('shippingType')}>
                    <option value="">Оберіть вид відправлення</option>
                    {shippingTypes.map((type) => (
                        <option key={type} value={type}>
                            {type}
                        </option>
                    ))}
                </select>
                {errors.shippingType && <ErrorMsg>{errors.shippingType.message}</ErrorMsg>}
            </FormGroup>
            {[...Array(placesCount)].map((_, index) => (
                <div key={index}>
                    <h3>Місце {index + 1}</h3>
                    <FormGroup>
                        <Label htmlFor={`places[${index}].quantity`}>Кількість:</Label>
                        <Input id={`places[${index}].quantity`} {...register(`places[${index}].quantity`)} type="number" />
                        {errors?.places?.[index]?.quantity && <ErrorMsg>{errors.places[index].quantity.message}</ErrorMsg>}
                    </FormGroup>
                    <FormGroup>
                        <Label htmlFor={`places[${index}].packaging`}>Пакування:</Label>
                        <Input id={`places[${index}].packaging`} {...register(`places[${index}].packaging`)} type="number" />
                        {errors?.places?.[index]?.packaging && <ErrorMsg>{errors.places[index].packaging.message}</ErrorMsg>}
                    </FormGroup>
                </div>
            ))}
            <FormGroup>
                <Label htmlFor="lift">Підйом на поверх:</Label>
                <Input id="floors" {...register('lift.floors')} type="number" placeholder="Кількість поверхів" />

                <Label htmlFor="elevator">Ліфт</Label>
                <Checkbox id="elevator" {...register('lift.elevator')} type="checkbox" />
                {errors.lift && <ErrorMsg>{errors.lift.message}</ErrorMsg>}
            </FormGroup>
            <FormGroup>
                <Label htmlFor="returnDelivery">Зворотна доставка:</Label>
                <Checkbox id="returnDelivery" {...register('returnDelivery')} type="checkbox" />
            </FormGroup>
            <div>
                <button type="submit">Реалізувати</button>
                <button type="button" onClick={handleReset}>Очистити</button>
                <button type="button" onClick={addPlace}>Додати місце</button>
            </div>
        </FormContainer>
    );
};

export default NovaForm;