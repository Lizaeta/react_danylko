import logo from './logo.svg';
import './App.css';
import Form from "./components/form";
import NovaForm from './components/novaform';
function App() {
  return (
      <div className="App">
        <h1>Форма зворотнього зв'язку</h1>
        <Form />

          <h1>Форма для розрахунку Вартості доставки</h1>
          <NovaForm />
      </div>


  );
}

export default App;
