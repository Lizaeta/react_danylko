import React, {useState, useEffect} from "react";
import {Image} from "react-bootstrap";

function PhotoItem(props) {
    const { photo, onClick } = props;
    return (
        <div className="list-group-item d-flex align-items-center p-2">
            <div className="mx-2">
                <Image roundedCircle={true} src={photo.thumbnailUrl} onClick={onClick}  />
            </div>
            <div className="mx-2">
                {props.photo.title}
            </div>


        </div>
    );


}

export default PhotoItem;