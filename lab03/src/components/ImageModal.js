import React, { useState } from 'react';

function ImageModal(props) {
    const { imageUrl, onClose } = props;

    return (
        <div className="modal-overlay ImgModal">
            <button type="button" className="btn btn-danger" onClick={onClose}>Close</button>
            <div className="modal-content">
                <img src={imageUrl} alt="Image" />
            </div>
        </div>
    );
}

export default ImageModal;

