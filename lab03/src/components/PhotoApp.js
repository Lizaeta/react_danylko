import React, {useState, useEffect} from "react";
import {List} from "react-virtualized";
import PhotoItem from "./PhotoItem";
import Container from "react-bootstrap/container";
import ImageModal from "./ImageModal";
function PhotoApp() {
    const [photos, setPhotos] = useState(null);
    const [photosBackup, setPhotosBackup] = useState(null);
    const [showModal, setShowModal] = useState(false);
    const [selectedImage, setSelectedImage] = useState('');

    useEffect(() => {
        fetchPhotos();
    }, []);

    const fetchPhotos = async () => {
        try {
            const response = await fetch("https://jsonplaceholder.typicode.com/photos");
            const photos = await response.json();
            setPhotos(photos);
            setPhotosBackup(photos)
        } catch (error) {
            console.error("Помилка завантаження фотографій", error);
        }
    };

    const openImage = (imageUrl) => {
        setSelectedImage(imageUrl);
        setShowModal(true);
    };

    const closeImage = () => {
        setSelectedImage('');
        setShowModal(false);
    };

    const handleTitleFilterChange = (e) => {
        const filteredPhotos = photos.filter(photo => photo.title.toLowerCase().includes(e.target.value.toLowerCase()))
        console.log(filteredPhotos)
        if (filteredPhotos == []) {
            fetchPhotos();
        } else {
            setPhotos(filteredPhotos);
        }
        if (e.target.value == "") {
            setPhotos(photosBackup);
            fetchPhotos();
        }
    };

    const handleSortChange = (e) => {
        console.log(e.target.value);
        switch (e.target.value) {
            case "albumId": {
                setPhotos(null)
                setPhotos(photos.sort((a, b) => {
                    if (a.albumId < b.albumId) {
                        return -1;
                    }
                    if (a.albumId > b.albumId) {
                        return 1;
                    }
                    return 0;
                }))
                break;
            }
            case "title": {
                setPhotos(null)
                setPhotos(photos.sort((a, b) => {
                    if (a.title < b.title) {
                        return -1;
                    }
                    if (a.title > b.title) {
                        return 1;
                    }
                    return 0;
                }))
                break;
            }
            case "id": {
                setPhotos(null)
                setPhotos(photos.sort((a, b) => {
                    if (a.id < b.id) {
                        return -1;
                    }
                    if (a.id > b.id) {
                        return 1;
                    }
                    return 0;
                }))
                break;
            }
        }
    };

    const rowRenderer = ({index, key, style}) => {
        const photo = photos[index];
        return (
            <div key={key} style={style}>
                <PhotoItem photo={photo} onClick={() => openImage(photo.url)}/>
            </div>
        );
    };

    return (
        <Container>
            <div>
                <label htmlFor="titleFilter">Фільтрація за заголовком:</label>
                <input
                    type="text"
                    id="titleFilter"
                    onChange={handleTitleFilterChange}
                />
            </div>
            <div>
                <label htmlFor="sort">Сортувати за:</label>
                <select id="sort" onChange={handleSortChange}>
                    <option value="id">ID</option>
                    <option value="albumId">Альбом</option>
                    <option value="title">Заголовок</option>
                </select>
            </div>
            <div id="menu" className="list-group">
                <List
                    width={600}
                    height={700}
                    rowCount={photos ? photos.length : 0}
                    rowHeight={170}
                    rowRenderer={rowRenderer}
                />
                {showModal && (
                    <ImageModal imageUrl={selectedImage} onClose={closeImage}/>
                )}
            </div>
        </Container>
    );
}

export default PhotoApp;
