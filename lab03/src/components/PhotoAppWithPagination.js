import React, { useEffect, useState } from "react";
import axios from "axios";
import { List } from "react-virtualized";
import "bootstrap/dist/css/bootstrap.min.css";
import Pagination from "react-bootstrap/Pagination";
import PhotoItem from "./PhotoItem";
import ImageModal from "./ImageModal";

export default function PhotoAppWithPagination() {
    const [state, setState] = useState({
        data: [],
        limit: 10,
        activePage: 1,
    });
    const [filteredData, setFilteredData] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [selectedImage, setSelectedImage] = useState("");
    const [titleFilter, setTitleFilter] = useState("");

    useEffect(() => {
        axios
            .get(
                `https://jsonplaceholder.typicode.com/photos?_page=1&_limit=${state.limit}`
            )
            .then((res) => {
                console.log(res.data);
                setState((prev) => ({
                    ...prev,
                    data: res.data,
                }));
            })
            .catch((error) => console.log(error));
    }, [state.limit]);

    useEffect(() => {
        // Функція, яка фільтрує дані за заголовком
        const filterData = () => {
            const filtered = state.data.filter((photo) =>
                photo.title.toLowerCase().includes(titleFilter.toLowerCase())
            );
            setFilteredData(filtered);
        };

        // Викликаємо фільтрацію при зміні titleFilter або state.data
        filterData();
    }, [titleFilter, state.data]);

    const handlePageChange = (pageNumber) => {
        setState((prev) => ({ ...prev, activePage: pageNumber }));

        axios
            .get(`https://jsonplaceholder.typicode.com/photos?_page=${pageNumber}`)
            .then((res) => {
                setState((prev) => ({
                    ...prev,
                    data: res.data,
                }));
            })
            .catch((error) => console.log(error));
    };

    const openImage = (imageUrl) => {
        setSelectedImage(imageUrl);
        setShowModal(true);
    };

    const closeImage = () => {
        setSelectedImage("");
        setShowModal(false);
    };

    const handleSortChange = (e) => {
        const sortBy = e.target.value;

        // Створюємо копію масиву даних
        const newData = [...state.data];

        switch (sortBy) {
            case "albumId":
                newData.sort((a, b) => a.albumId - b.albumId);
                break;
            case "title":
                newData.sort((a, b) => a.title.localeCompare(b.title));
                break;
            case "id":
                newData.sort((a, b) => a.id - b.id);
                break;
            default:
                break;
        }

        setState((prev) => ({
            ...prev,
            data: newData,
        }));
    };

    const rowRenderer = ({ index, key, style }) => {
        const photo = filteredData[index] || state.data[index];
        return (
            <div key={key} style={style}>
                <PhotoItem photo={photo} onClick={() => openImage(photo.url)} />
            </div>
        );
    };

    const handleTitleFilterChange = (e) => {
        setTitleFilter(e.target.value);
    };

    return (
        <div className="App">
            <div>
                <label htmlFor="titleFilter">Фільтрація за заголовком:</label>
                <input
                    type="text"
                    id="titleFilter"
                    onChange={handleTitleFilterChange}
                    value={titleFilter}
                />
            </div>
            <div>
                <label htmlFor="sort">Сортувати за:</label>
                <select id="sort" onChange={handleSortChange}>
                    <option value="id">ID</option>
                    <option value="albumId">Альбом</option>
                    <option value="title">Заголовок</option>
                </select>
            </div>
            <div id="menu" className="list-group">
                <List
                    width={600}
                    height={700}
                    rowCount={filteredData.length || state.data.length}
                    rowHeight={170}
                    rowRenderer={rowRenderer}
                />
                {showModal && (
                    <ImageModal imageUrl={selectedImage} onClose={closeImage} />
                )}
            </div>
            <Pagination className="px-4">
                {state.data.map((_, index) => {
                    return (
                        <Pagination.Item
                            onClick={() => handlePageChange(index + 1)}
                            key={index + 1}
                            active={index + 1 === state.activePage}
                        >
                            {index + 1}
                        </Pagination.Item>
                    );
                })}
            </Pagination>
        </div>
    );
}