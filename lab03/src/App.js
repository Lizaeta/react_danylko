import logo from './logo.svg';
import './App.css';
import PhotoApp from "./components/PhotoApp";
import PhotoAppWithPagination from "./components/PhotoAppWithPagination";

function App() {
  return (
    <div className="App">
    <PhotoAppWithPagination/>

    </div>
  );
}

export default App;
